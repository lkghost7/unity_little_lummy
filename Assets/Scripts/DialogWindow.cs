﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogWindow : MonoBehaviour
{
    [SerializeField] private GameObject textDialogWindow;
    [SerializeField] private GameObject closeBtn;
    private int _childCount;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseWindow();
        }
    }

    private void Start()
    {
        textDialogWindow.GetComponent<TextMeshPro>().SetText("");
        _childCount = transform.childCount;
    }
    public void OpenWindow()
    {
        transform.GetComponent<Renderer>().enabled = true;
        SwitсhWindow(true);
    }
    public void CloseWindow()
    {
        SetText("");
        SwitсhWindow(false);
    }

    public void SetText(string text)
    {
        textDialogWindow.GetComponent<TextMeshPro>().SetText(text);
    }

    private void SwitсhWindow(bool switchOnOrSwitchOff)
    {
        transform.GetComponent<Renderer>().enabled = switchOnOrSwitchOff;
        for (var i = 0; i < _childCount; i++)
        {
            var obj = transform.GetChild(i).gameObject;
            obj.GetComponent<Renderer>().enabled = switchOnOrSwitchOff;
        }
    }
}
