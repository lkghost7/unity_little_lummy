﻿using UnityEngine;

public class TriggerPickUP : MonoBehaviour
{
    [SerializeField] private DialogWindow dialogWindow;
    [SerializeField] private ItemBar itemBar;
    
    private bool isHeroNear;
    private int countView;

    private void Start()
    {
//        PlayerPrefs.DeleteKey("HammerCount");
        countView = 0;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isHeroNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        isHeroNear = false;
    }

    private void Update()
    {
        if (transform.name == "Hammer")
        {
            ShowText("Таким молотком можно разбивать камни! Кто знает, что внутри...");
            
//           ShowText("За все предыдущие запуски игры вы брали этот молот уже: " + GetSavedCount() + " раз.");
        }
    }

    private void ShowText(string textMessage)
    {
        if (isHeroNear && Input.GetKeyDown(KeyCode.E))
        {
            dialogWindow.SetText(textMessage);
            dialogWindow.OpenWindow();
            TakeItem();
        }
    }

    private void TakeItem()
    {
        itemBar.SetItemToFreeCell(gameObject);
    }

//    private void ShowText(string textMessage)
//    {
//        if (isHeroNear && Input.GetKeyDown(KeyCode.E))
//        {
//            countView = GetSavedCount(); 
//            SaveCount();
//            dialogWindow.SetText(textMessage);
//            dialogWindow.OpenWindow();
//        }
//    }
//        void SaveCount() {
//            PlayerPrefs.SetInt(
//                "HammerCount", countView + 1);
//        }

//        private int GetSavedCount()
//        {
//            return PlayerPrefs.GetInt("HammerCount");
//        }
}