﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LummyCamera : MonoBehaviour
{
    [SerializeField] private Transform m_Target;
    [SerializeField] private float m_Distance = -9f;
    [SerializeField] private float m_Height = 6f;
    
    void Update()
    {
        var playerPosition = new Vector3(m_Target.position.x, m_Target.position.y + m_Height, m_Target.position.z + m_Distance);
        transform.position = Vector3.Lerp(transform.position, playerPosition, Time.deltaTime * 2f);
    }
}
