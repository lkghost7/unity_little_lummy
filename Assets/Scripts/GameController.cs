﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _leftHand;
    public static GameController INSTANCE = null;
    [HideInInspector] public bool playerTurn = true;
    public int playerHealth = 100;

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else if (INSTANCE != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        InitGame();
    }

    void InitGame()
    {
        
    }

    
    
        
    
    public void GameOver()
    {
        enabled = false;
    }
}
