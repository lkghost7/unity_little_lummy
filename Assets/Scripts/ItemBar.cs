﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBar : MonoBehaviour
{
    public GameObject[] itemsBar;

    public void SetItemToFreeCell(GameObject obj)
    {
        for (var i = 0; i < itemsBar.Length; i++)
        {
                obj.transform.position = itemsBar[i].transform.position;
                itemsBar[i] = obj;
        }
    }

    public GameObject GetItemFromItemsBar(int index)
    {
        return itemsBar[index];
    }
}
