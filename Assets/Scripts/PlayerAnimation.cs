﻿using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator _animator;
    private static readonly int PlayerRunRight = Animator.StringToHash("PlayerRunRight");
    private static readonly int PlayerRunLeft = Animator.StringToHash("PlayerRunLeft");
    private static readonly int PlayerAttack = Animator.StringToHash("PlayerAttack");
    private static readonly int PlayerIdle = Animator.StringToHash("PlayerIdle");
    
    private bool _facingRight = true;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (_facingRight)
            {
                FlipHorizontal();
                _facingRight = false;
            }
            _animator.SetTrigger(PlayerRunLeft);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            _animator.SetTrigger(PlayerIdle);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            if (!_facingRight)
            {
                FlipHorizontal();
                _facingRight = true;
            }
            _animator.SetTrigger(PlayerRunRight);
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            _animator.SetTrigger(PlayerIdle);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            _animator.SetTrigger(PlayerAttack);
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            _animator.SetTrigger(PlayerIdle);
        }
    }

    void FlipHorizontal()
    {
        _animator.transform.Rotate(0, 180, 0);
    }
}