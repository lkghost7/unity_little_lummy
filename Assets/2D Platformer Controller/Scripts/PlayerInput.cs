﻿using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
    private Player _player;
    
    

    private void Start()
    {
        _player = GetComponent<Player>();
    }

    private void Update()
    {
        Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        _player.SetDirectionalInput(directionalInput);

        if (Input.GetButtonDown("Jump"))
        {
            _player.OnJumpInputDown();
        }

        if (Input.GetButtonUp("Jump"))
        {
            _player.OnJumpInputUp();
        }
    }
}
